<h2>Installation steps</h2>

<p>
Need global package "babel-cli", "browserify"
</p>
<code>npm i --save-dev react</code><br>
<code>npm i --save-dev react-dom</code><br>
<code>npm i --save-dev prop-types</code>
<code>npm i --save-dev classnames</code><br>
<code>npm i --save-dev babel-preset-es2015</code><br>
<code>npm i --save-dev babel-preset-react</code><br>
<code>npm i --save-dev whatwg-fetch</code><br>
<code>npm i --save-dev react-tinymce</code><br>
<code>npm i --save-dev babelify</code><br>
<code>npm i --save-dev fbemitter</code><br>
OR<br>
<code>npm i --save-dev react react-dom prop-types classnames babel-preset-es2015 babel-preset-react whatwg-fetch react-tinymce babelify fbemitter</code>

Dynamic create files on directory<br> 
- js/build/*
- bundle.js
- bundle.css