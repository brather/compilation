"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _AppStorage = require("./AppStorage");

var _AppStorage2 = _interopRequireDefault(_AppStorage);

require("whatwg-fetch");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by AChechel on 06.06.2017.
 */
var AppAction = {
    addPreviewBlock: function addPreviewBlock(content) {
        var tmp = document.createElement('DIV');
        tmp.innerHTML = content;
        tmp = tmp.textContent.substr(0, 75) + "...";

        var data = _AppStorage2.default.getData();
        var arr = [];

        if ('previewBlock' in data) {
            arr = data.previewBlock;
        }

        if (arr.indexOf(tmp) < 0) {
            arr.push(tmp);
            _AppStorage2.default.setData(arr, 'previewBlock');
        }
    },


    /* localStorage Object contentSources */
    putContentOnSourceArea: function putContentOnSourceArea(sourceId) {
        var cont = _AppStorage2.default.getRecord('contentSources');
        if (!cont || !cont[sourceId]) {
            fetch("/api.php?action=parse&pageid=" + sourceId + "&format=json").then(function (response) {
                /* Check Status*/
                if (response.status >= 200 && response.status < 300) {
                    return response;
                } else {
                    var error = new Error(response.statusText);
                    error.response = response;
                    throw error;
                }
            }).then(function (parseJSON) {
                return parseJSON.json();
            }).then(function (data) {
                if (data.error) {
                    var error = new Error(data.error.info);
                    error.responce = data.error;
                    throw error;
                } else {
                    var obj = {};
                    var sourceArea = _AppStorage2.default.getRecord("sourceArea");

                    if (cont) obj = cont;

                    obj[sourceId] = data.parse.text["*"];
                    if (sourceArea) obj[sourceArea.originId] = sourceArea.content;

                    _AppStorage2.default.setData(obj, "contentSources", false);

                    _AppStorage2.default.setData({
                        content: obj[sourceId],
                        originId: sourceId
                    }, "sourceArea");
                }
            });
        } else {
            var sourceArea = _AppStorage2.default.getRecord("sourceArea");
            cont[sourceArea.originId] = sourceArea.content;

            _AppStorage2.default.setData(cont, "contentSources", false);

            _AppStorage2.default.setData({
                content: cont[sourceId],
                originId: sourceId
            }, "sourceArea");
        }
    },
    appendContentCompilationResource: function appendContentCompilationResource(content) {
        var resource = _AppStorage2.default.getRecord('compilationResource');
        if (resource) {
            /* добавим контент если его еще нет в ресурсе */
            if (resource.indexOf(content) < 0) {
                content = resource + content;
            } else {
                return true;
            }
        }

        _AppStorage2.default.setData(content, 'compilationResource');
    }
};

exports.default = AppAction;