"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

require("whatwg-fetch");

var _fbemitter = require("fbemitter");

/** @type Array<Object> */
/**
 * Created by AChechel on 06.06.2017.
 */
var data = {};

var emitter = new _fbemitter.EventEmitter();

var AppStorage = {

    /**
     * @param eventType string
     * @param fn Function   */
    addListener: function addListener(eventType, fn) {
        _l("AppStorage::addListener", eventType);
        emitter.addListener(eventType, fn);
    },


    /** @return Array<Object> */
    init: function init(param) {
        this.param = param;

        var storage = 'localStorage' in window ? localStorage.getItem('data') : null;

        if (storage === null || storage === "null") {
            this._ajaxSource(param.SourceArea.originId);
            this._ajaxTabs(param.ContentTabs);
        } else {
            data = JSON.parse(storage);
            if ("sourceArea" in data && data.sourceArea.originId !== param.SourceArea.originId || !("sourceArea" in data)) {
                this._ajaxSource(param.SourceArea.originId);
            }

            if (!("contentTabs" in data)) {
                this._ajaxTabs(param.ContentTabs);
            }

            _l("AppStorage::init data, param", data, param);
        }
    },
    _ajaxTabs: function _ajaxTabs(param) {
        var _this = this;

        _l("start _ajaxTabs");
        var uri = "http://172.16.61.56/services/api/search/titles/search" + "?q=" + param.title + "&limit=" + param.limit + "&preview=" + param.preview;
        fetch(uri).then(function (response) {
            /* Check Status*/
            if (response.status >= 200 && response.status < 300) {
                return response;
            } else {
                var error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        }).then(function (parseJSON) {
            return parseJSON.json();
        }).then(function (data) {

            if (data.error) {
                var error = new Error(data.error.info);
                error.responce = data.error;
                throw error;
            } else {
                var obTabs = {
                    articles: data.articles,
                    count: data.count,
                    total_count: data.total_count
                };
                _this.setData(obTabs, "contentTabs");
                emitter.emit('contentTabs');
            }
        });
    },
    _ajaxSource: function _ajaxSource(originID) {
        var _this2 = this;

        _l("start _ajaxSource");
        fetch("/api.php?action=parse&pageid=" + originID + "&format=json").then(function (response) {
            /* Check Status*/
            if (response.status >= 200 && response.status < 300) {
                return response;
            } else {
                var error = new Error(response.statusText);
                error.response = response;
                throw error;
            }
        }).then(function (parseJSON) {
            return parseJSON.json();
        }).then(function (data) {

            if (data.error) {
                var error = new Error(data.error.info);
                error.responce = data.error;
                throw error;
            } else {
                var obText = {
                    content: data.parse.text["*"].replace(/<!--[\s\S]*?-->/gm, ''),
                    originId: originID
                };
                _this2.setData(obText, "sourceArea");
                emitter.emit('sourceArea');
            }
        });
    },

    /** @return Array<Object> */
    getData: function getData() {
        return data;
    },


    /**
     *  @param newData Object
     *  @param key string
     *  @param commit boolean    */
    setData: function setData(newData, key) {
        var commit = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;


        if ('localStorage' in window) {
            data[key] = newData;
            localStorage.setItem('data', JSON.stringify(data));
            /* На каждый обект в localStorage data должен приходится свой Listener с именем key */
            if (commit) emitter.emit(key);
        }
    },


    /**
     * @param key number
     * @return Object|null */
    getRecord: function getRecord(key) {
        if (!data) return {};

        return key in data ? data[key] : null;
    }
};

exports.default = AppStorage;