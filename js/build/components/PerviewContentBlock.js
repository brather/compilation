"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _AppStorage = require("../flux/AppStorage");

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 02.06.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var PreviewContentBlock = function (_Component) {
    _inherits(PreviewContentBlock, _Component);

    function PreviewContentBlock(props) {
        _classCallCheck(this, PreviewContentBlock);

        var _this = _possibleConstructorReturn(this, (PreviewContentBlock.__proto__ || Object.getPrototypeOf(PreviewContentBlock)).call(this, props));

        _this.state = {
            blocks: _AppStorage2.default.getRecord("previewBlock") || []
        };

        _AppStorage2.default.addListener("previewBlock", function () {
            _this.setState({
                blocks: _AppStorage2.default.getRecord("previewBlock")
            });
        });
        return _this;
    }

    _createClass(PreviewContentBlock, [{
        key: "render",
        value: function render() {
            _l("PreviewContentBlock::render");
            var previewBlocks = [];

            if (this.state.blocks && this.state.blocks.length) {

                for (var i = 0; i < this.state.blocks.length; i++) {

                    previewBlocks.push(_react2.default.createElement(
                        "div",
                        { className: "new-source-item", key: i },
                        _react2.default.createElement(
                            "div",
                            null,
                            this.state.blocks[i]
                        ),
                        _react2.default.createElement(
                            "span",
                            { className: "new-source-item-number" },
                            i + 1
                        )
                    ));
                }
            } else {
                previewBlocks.push(_react2.default.createElement(
                    "div",
                    { className: "empty-preview", key: "0" },
                    "\u041D\u0438\u0447\u0435\u0433\u043E \u043D\u0435 \u0432\u044B\u0431\u0440\u0430\u043D\u043E"
                ));
            }

            return _react2.default.createElement(
                "div",
                { className: "col-12" },
                previewBlocks.map(function (v) {
                    return v;
                })
            );
        }
    }]);

    return PreviewContentBlock;
}(_react.Component);

exports.default = PreviewContentBlock;