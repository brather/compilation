'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactTinymce = require('react-tinymce');

var _reactTinymce2 = _interopRequireDefault(_reactTinymce);

var _AppStorage = require('../flux/AppStorage');

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 31.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var CompilationResource = function (_Component) {
    _inherits(CompilationResource, _Component);

    function CompilationResource(props) {
        _classCallCheck(this, CompilationResource);

        var _this = _possibleConstructorReturn(this, (CompilationResource.__proto__ || Object.getPrototypeOf(CompilationResource)).call(this, props));

        _l('CompilationResource::constructor');

        _this.state = {
            resource: _AppStorage2.default.getRecord('compilationResource') || ""
        };

        _AppStorage2.default.addListener("compilationResource", function () {
            _this.setState({
                resource: _AppStorage2.default.getRecord('compilationResource')
            });
        });
        return _this;
    }

    _createClass(CompilationResource, [{
        key: 'render',
        value: function render() {
            _l('CompilationResource::render');

            return _react2.default.createElement(_reactTinymce2.default, {
                content: this.state.resource,
                config: {
                    height: 400,
                    menubar: false,
                    branding: false,
                    resize: false
                },
                onChange: this.handleChange.bind(this),
                id: this.props.id,
                ref: this.props.refs
            });
        }
    }, {
        key: 'handleChange',
        value: function handleChange(e) {
            var res = _AppStorage2.default.getRecord("compilationResource");
            var con = tinyMCE.EditorManager.get(this.props.id).getContent();

            if (res !== con) _AppStorage2.default.setData(con, "compilationResource", false);
        }
    }, {
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(newProps, newState) {
            if (this.state.resource !== newState.resource) {
                tinyMCE.EditorManager.get(this.props.id).setContent(newState.resource);
                return true;
            }

            return false;
        }

        /*componentDidUpdate() {
              _l('CompilationResource::componentDidUpdate');
              if (this.props.resource){
                let tmpRes = tinyMCE.EditorManager.get(this.props.id).getContent() + this.props.resource;
                tinyMCE.EditorManager.get(this.props.id).setContent(tmpRes);
            }
        }*/

    }]);

    return CompilationResource;
}(_react.Component);

CompilationResource.defaultProps = {
    className: 'source-area',
    id: 'CompilationResource',
    refs: "compilationResource"
};

exports.default = CompilationResource;