'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _ContentUlLi = require('./ContentUlLi');

var _ContentUlLi2 = _interopRequireDefault(_ContentUlLi);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _AppStorage = require('../flux/AppStorage');

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 29.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var ContentTabs = function (_Component) {
    _inherits(ContentTabs, _Component);

    function ContentTabs(props) {
        _classCallCheck(this, ContentTabs);

        var _this = _possibleConstructorReturn(this, (ContentTabs.__proto__ || Object.getPrototypeOf(ContentTabs)).call(this, props));

        _l('ContentTabs::constructor');

        _this.state = _AppStorage2.default.getRecord("contentTabs") || {};

        _AppStorage2.default.addListener("contentTabs", function () {
            _this.setState(function () {
                return _AppStorage2.default.getRecord("contentTabs");
            });
        });
        return _this;
    }

    _createClass(ContentTabs, [{
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(newProp, newState) {
            _l('ContentTabs::shouldComponentUpdate', newProp, newState);
            return this.state.count !== newState.count;
        }
    }, {
        key: 'render',
        value: function render() {
            _l('ContentTabs::render', this.state);

            var list = [];

            for (var i = 0; i < this.state.count; i++) {
                var contentUlLi = _react2.default.createElement(_ContentUlLi2.default, {
                    article: this.state.articles[i],
                    key: i
                });
                /* сравнение строковых значений ID */
                if (this.state.articles[i].id === _AppStorage2.default.param.SourceArea.originId) {
                    list.unshift(_react2.default.createElement(_ContentUlLi2.default, {
                        article: this.state.articles[i],
                        key: i,
                        className: 'active-li'
                    }));
                } else {
                    list.push(contentUlLi);
                }
            }

            return _react2.default.createElement(
                'ul',
                { className: 'content-tabs' },
                list.map(function (v) {
                    return v;
                })
            );
        }
    }]);

    return ContentTabs;
}(_react.Component);

ContentTabs.propsType = {
    articles: _propTypes2.default.arrayOf({
        category_id: _propTypes2.default.any,
        full_title: _propTypes2.default.string,
        id: _propTypes2.default.string,
        is_fuzzy: _propTypes2.default.number,
        picture: _propTypes2.default.any,
        preview: _propTypes2.default.string,
        source_id: _propTypes2.default.number,
        source_name: _propTypes2.default.string,
        title: _propTypes2.default.string
    }),
    count: _propTypes2.default.number,
    total_count: _propTypes2.default.number,
    originId: _propTypes2.default.string
};

exports.default = ContentTabs;