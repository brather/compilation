'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * Created by AChechel on 01.06.2017.
 */

var deepDiffMapper = function () {
    return {
        VALUE_CREATED: 'created',
        VALUE_UPDATED: 'updated',
        VALUE_DELETED: 'deleted',
        VALUE_UNCHANGED: 'unchanged',
        map: function map(obj1, obj2) {
            if (this.isFunction(obj1) || this.isFunction(obj2)) {
                throw 'Invalid argument. Function given, object expected.';
            }
            if (this.isValue(obj1) || this.isValue(obj2)) {
                return {
                    type: this.compareValues(obj1, obj2),
                    data: obj1 === undefined ? obj2 : obj1
                };
            }

            var diff = {};
            for (var key in obj1) {
                if (this.isFunction(obj1[key])) {
                    continue;
                }

                var value2 = undefined;
                if ('undefined' !== typeof obj2[key]) {
                    value2 = obj2[key];
                }

                diff[key] = this.map(obj1[key], value2);
            }

            for (var _key in obj2) {
                if (this.isFunction(obj2[_key]) || 'undefined' !== typeof diff[_key]) {
                    continue;
                }

                diff[_key] = this.map(undefined, obj2[_key]);
            }

            return diff;
        },
        compareValues: function compareValues(value1, value2) {
            if (value1 === value2) {
                return this.VALUE_UNCHANGED;
            }
            if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {
                return this.VALUE_UNCHANGED;
            }
            if ('undefined' === typeof value1) {
                return this.VALUE_CREATED;
            }
            if ('undefined' === typeof value2) {
                return this.VALUE_DELETED;
            }

            return this.VALUE_UPDATED;
        },
        isFunction: function isFunction(obj) {
            return {}.toString.apply(obj) === '[object Function]';
        },
        isArray: function isArray(obj) {
            return {}.toString.apply(obj) === '[object Array]';
        },
        isDate: function isDate(obj) {
            return {}.toString.apply(obj) === '[object Date]';
        },
        isObject: function isObject(obj) {
            return {}.toString.apply(obj) === '[object Object]';
        },
        isValue: function isValue(obj) {
            return !this.isObject(obj) && !this.isArray(obj);
        }
    };
}();

exports.default = deepDiffMapper;