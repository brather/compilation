'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactTinymce = require('react-tinymce');

var _reactTinymce2 = _interopRequireDefault(_reactTinymce);

var _AppStorage = require('../flux/AppStorage');

var _AppStorage2 = _interopRequireDefault(_AppStorage);

var _AppAction = require('../flux/AppAction');

var _AppAction2 = _interopRequireDefault(_AppAction);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 26.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var SourceArea = function (_Component) {
    _inherits(SourceArea, _Component);

    function SourceArea(props) {
        _classCallCheck(this, SourceArea);

        var _this = _possibleConstructorReturn(this, (SourceArea.__proto__ || Object.getPrototypeOf(SourceArea)).call(this, props));

        _l('SourceArea::constructor');

        _this.state = _AppStorage2.default.getRecord("sourceArea") || { content: "Loading" };

        _AppStorage2.default.addListener("sourceArea", function () {
            _this.setState(function () {
                return _AppStorage2.default.getRecord("sourceArea") || { content: "Что то пошло не так!" };
            });
        });
        return _this;
    }

    _createClass(SourceArea, [{
        key: 'render',
        value: function render() {
            _l('SourceArea::render');
            return _react2.default.createElement(_reactTinymce2.default, {
                content: this.state.content,
                config: {
                    height: 395,
                    menubar: false,
                    plugins: [],
                    toolbar: 'copy',
                    content_css: ['//fonts.googleapis.com/css?family=Lato:300,300i,400,400i', '//www.tinymce.com/css/codepen.min.css'],
                    resize: false,
                    branding: false,
                    statusbar: false
                },
                onCopy: this.handleCopy.bind(this),
                ref: this.props.refs,
                id: this.props.id
            });
        }
    }, {
        key: 'handleCopy',
        value: function handleCopy(e) {
            _AppAction2.default.addPreviewBlock(tinyMCE.EditorManager.get(this.props.id).selection.getContent());
            _AppAction2.default.appendContentCompilationResource('<p>' + tinyMCE.EditorManager.get(this.props.id).selection.getContent() + '</p>');
        }
    }, {
        key: 'onSelection',
        value: function onSelection(e) {
            _l('SourceArea::onSelection target', e.target);
        }
    }, {
        key: 'shouldComponentUpdate',
        value: function shouldComponentUpdate(newProps, newState) {
            _l('SourceArea::shouldComponentUpdate');
            if (newState.originId !== this.state.originId) {
                tinyMCE.EditorManager.get(this.props.id).setContent(newState.content);
                return true;
            }

            return false;
        }

        /*_handleUpdateState () {
            this.setState();
        }*/

    }]);

    return SourceArea;
}(_react.Component);

SourceArea.defaultProps = {
    handleAppendResource: undefined,
    parse: {},
    refs: "sourceArea",
    className: "source-area",
    id: "source-area"
};

exports.default = SourceArea;