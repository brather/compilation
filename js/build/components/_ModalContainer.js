"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _SourceArea = require("./SourceArea");

var _SourceArea2 = _interopRequireDefault(_SourceArea);

var _ContentTabs = require("./ContentTabs");

var _ContentTabs2 = _interopRequireDefault(_ContentTabs);

var _CompilationResource = require("./CompilationResource");

var _CompilationResource2 = _interopRequireDefault(_CompilationResource);

var _PerviewContentBlock = require("./PerviewContentBlock");

var _PerviewContentBlock2 = _interopRequireDefault(_PerviewContentBlock);

var _AppStorage = require("../flux/AppStorage");

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 31.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var ModalContainer = function (_Component) {
    _inherits(ModalContainer, _Component);

    function ModalContainer(props) {
        _classCallCheck(this, ModalContainer);

        var _this = _possibleConstructorReturn(this, (ModalContainer.__proto__ || Object.getPrototypeOf(ModalContainer)).call(this, props));

        _this.state = {
            CompilationResource: "",
            needUpdate: true,
            previewContentBlocks: []
        };

        /* Эта переменная должна быть установлена самой первой и загрузиться должна из контектса страницы */
        _this.originId = "1354279";

        _this._handleSelectedSourceArea = _this._handleSelectedSourceArea.bind(_this);
        return _this;
    }

    _createClass(ModalContainer, [{
        key: "_handleSelectedSourceArea",
        value: function _handleSelectedSourceArea(sourceToAppend) {
            var _this2 = this;

            /*_l('ModalContainer::_handleSelectedSourceArea');*/

            this.setState(function () {
                var obj = {
                    CompilationResource: sourceToAppend,
                    needUpdate: false,
                    previewContentBlocks: _this2.state.previewContentBlocks
                };

                /* _l("On_handleSelectedSourceArea::state status", this.state);*/

                obj.previewContentBlocks.push({
                    textPreview: sourceToAppend.substr(0, 55) + "...",
                    numberBlock: _this2.state.previewContentBlocks.length
                });

                return obj;
            });
        }
    }, {
        key: "shouldComponentUpdate",
        value: function shouldComponentUpdate(newProp, newState) {
            /* Проверка событий при которых Модальное окно не нужно обновлять */
            /*_l('ModalContainer::shouldComponentUpdate');*/
            if (newState.previewContentBlocks.length > this.state.previewContentBlocks) return true;

            return true;
        }
    }, {
        key: "_fetchSourceArea",
        value: function _fetchSourceArea(originId) {
            if (1 * this.originId === 1 * originId) return false;

            this.fetchSourceArea.state = { url: "/api.php?action=parse&pageid=" + originId + "&format=json" };
        }
    }, {
        key: "_renderSourceArea",
        value: function _renderSourceArea() {

            return _react2.default.createElement(_SourceArea2.default, { contentSource: "Loading" });
            /*return (
                <Fetch url={"/api.php?action=parse&pageid=" + this.originId + "&format=json"} ref={(fe) => {this.fetchSourceArea = fe} } >
                    <SourceArea contentSource={"Loading"} handleAppendResource={this._handleSelectedSourceArea} />
                </Fetch>
            );*/
        }
    }, {
        key: "_renderTabs",
        value: function _renderTabs() {
            return _react2.default.createElement(_ContentTabs2.default, { handleOnClickLi: this._fetchSourceArea });
            /*<Fetch url={ uriFetch + "?q=" + this.props.title +
             "&limit=" + this.props.limit +
             "&preview=" + this.props.preview }>
             <ContentTabs  articles={[]} count={0} total_count={0} originId={this.originId} handleOnClickLi={this._fetchSourceArea} />
             </Fetch>*/
        }
    }, {
        key: "render",
        value: function render() {
            _l('ModalContainer::render', this.state);

            return _react2.default.createElement(
                "div",
                null,
                _react2.default.createElement(
                    "div",
                    { className: "row" },
                    _react2.default.createElement(
                        "div",
                        { className: "col-9" },
                        this._renderSourceArea()
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "col-3 list-tabs-container" },
                        this._renderTabs()
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { className: "row" },
                    _react2.default.createElement(
                        "div",
                        { className: "col-12" },
                        _react2.default.createElement(
                            "h2",
                            null,
                            "\u041D\u043E\u0432\u0430\u044F \u0441\u0442\u0430\u0442\u044C\u044F"
                        )
                    )
                ),
                _react2.default.createElement(
                    "div",
                    { className: "row" },
                    _react2.default.createElement(_PerviewContentBlock2.default, { blocks: this.state.previewContentBlocks })
                ),
                _react2.default.createElement(
                    "div",
                    { className: "row" },
                    _react2.default.createElement(
                        "h3",
                        null,
                        "\u041F\u0440\u0435\u0434\u043F\u0440\u043E\u0441\u043F\u043E\u0442\u0440"
                    ),
                    _react2.default.createElement(
                        "div",
                        { className: "col-12" },
                        _react2.default.createElement(_CompilationResource2.default, { resource: this.state.CompilationResource })
                    )
                )
            );
        }
    }]);

    return ModalContainer;
}(_react.Component);

exports.default = ModalContainer;