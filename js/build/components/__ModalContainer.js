"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _SourceArea = require("./SourceArea");

var _SourceArea2 = _interopRequireDefault(_SourceArea);

var _ContentTabs = require("./ContentTabs");

var _ContentTabs2 = _interopRequireDefault(_ContentTabs);

var _CompilationResource = require("./CompilationResource");

var _CompilationResource2 = _interopRequireDefault(_CompilationResource);

var _PerviewContentBlock = require("./PerviewContentBlock");

var _PerviewContentBlock2 = _interopRequireDefault(_PerviewContentBlock);

var _AppStorage = require("../flux/AppStorage");

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 31.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var ModalContainer = function (_Component) {
    _inherits(ModalContainer, _Component);

    function ModalContainer() {
        _classCallCheck(this, ModalContainer);

        return _possibleConstructorReturn(this, (ModalContainer.__proto__ || Object.getPrototypeOf(ModalContainer)).apply(this, arguments));
    }

    _createClass(ModalContainer, [{
        key: "_renderSourceArea",
        value: function _renderSourceArea() {
            return _react2.default.createElement(_SourceArea2.default, { contentSource: "Loading" });
        }
    }, {
        key: "_renderTabs",
        value: function _renderTabs() {
            return _react2.default.createElement(_ContentTabs2.default, { handleOnClickLi: this._fetchSourceArea });
        }
    }, {
        key: "render",
        value: function render() {
            _l('ModalContainer::render', this.state);

            return _react2.default.createElement(
                "div",
                null,
                _react2.default.createElement(
                    "div",
                    { className: "row" },
                    _react2.default.createElement(
                        "div",
                        { className: "col-9" },
                        this._renderSourceArea()
                    )
                )
            );
        }
    }]);

    return ModalContainer;
}(_react.Component);

exports.default = ModalContainer;