'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _tinymce = require('tinymce');

var _tinymce2 = _interopRequireDefault(_tinymce);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 18.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */


var Text = function (_Component) {
    _inherits(Text, _Component);

    function Text(props) {
        _classCallCheck(this, Text);

        var _this = _possibleConstructorReturn(this, (Text.__proto__ || Object.getPrototypeOf(Text)).call(this, props));

        _this.state = {
            porperty: {
                cols: _this.props.cols,
                rows: _this.props.rows,
                name: _this.props.name,
                id: _this.props.id
            },
            value: _this.props.defaultValue
        };
        return _this;
    }

    _createClass(Text, [{
        key: 'render',
        value: function render() {

            //this._renderTinyMCE();
            var comm = this.state.property;

            return _react2.default.createElement(
                'textarea',
                _extends({}, comm, { value: this.state.value }),
                this.state.value
            );
        }
    }, {
        key: '_renderTinyMCE',
        value: function _renderTinyMCE() {
            _tinymce2.default.init({
                selector: this.props.id,
                height: 500,
                plugins: ['advlist autolink lists link image charmap print preview anchor', 'searchreplace visualblocks code fullscreen', 'insertdatetime media table contextmenu paste code'],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                content_css: '//www.tinymce.com/css/codepen.min.css'
            });
        }
    }]);

    return Text;
}(_react.Component);

Text.porpTypes = {
    name: _propTypes2.default.string.isRequired,
    cols: _propTypes2.default.string,
    rows: _propTypes2.default.string,
    id: _propTypes2.default.string,
    defaultValue: _propTypes2.default.string
};

Text.defaultProps = {
    cols: "30",
    rows: "20"
};

exports.default = Text;