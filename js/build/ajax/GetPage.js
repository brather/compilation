'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactFetch = require('react-fetch');

var _reactFetch2 = _interopRequireDefault(_reactFetch);

var _TestComponent = require('../components/TestComponent');

var _TestComponent2 = _interopRequireDefault(_TestComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /**
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                * Created by AChechel on 16.05.2017.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                */

var GetPage = function (_React$Component) {
    _inherits(GetPage, _React$Component);

    function GetPage(props) {
        _classCallCheck(this, GetPage);

        return _possibleConstructorReturn(this, (GetPage.__proto__ || Object.getPrototypeOf(GetPage)).call(this, props));
    }

    _createClass(GetPage, [{
        key: 'render',
        value: function render() {
            return _react2.default.createElement(
                _reactFetch2.default,
                { url: this.props.url },
                _react2.default.createElement(_TestComponent2.default, null)
            );
        }
    }]);

    return GetPage;
}(_react2.default.Component);

exports.default = GetPage;