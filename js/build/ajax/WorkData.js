'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by AChechel on 17.05.2017.
 */

var WorkData = function () {
    function WorkData() {
        _classCallCheck(this, WorkData);
    }

    _createClass(WorkData, null, [{
        key: 'getResult',
        value: function getResult(response) {
            console.debug(response);
        }
    }, {
        key: 'resText',
        value: function resText(data) {
            if ((typeof data === 'undefined' ? 'undefined' : _typeof(data)) !== 'object') throw new Error('Response is not a Object');

            window.SourceArea.setState({ contentSource: data.parse.text });
        }
    }, {
        key: 'errorHandler',
        value: function errorHandler(err) {
            console.debug(err);
            throw new Error();
        }
    }]);

    return WorkData;
}();

exports.default = WorkData;