'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _ModalContainer = require('./components/ModalContainer');

var _ModalContainer2 = _interopRequireDefault(_ModalContainer);

var _AppStorage = require('./flux/AppStorage');

var _AppStorage2 = _interopRequireDefault(_AppStorage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//region/* Debug */
window.develop = false;
window._getLocal = function () {
    return JSON.parse(localStorage.getItem('data'));
};

window._l = window.develop ? console.log : function () {
    return void 0;
};
//endregion

_AppStorage2.default.init({
    SourceArea: {
        originId: "1354279"
    },
    ContentTabs: { /* Данные должны быть получены из контекста */
        title: "Гагарин Юрий Алексеевич",
        limit: 10,
        preview: 55
    }
});

_reactDom2.default.render(_react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(_ModalContainer2.default, null)
), document.getElementById("compilation"));
//region /* Module to resize modal container */
var modalBox = function modalBox() {
    return document.getElementsByClassName('modal-box')[0];
};
var modalContainer = function modalContainer() {
    return document.getElementsByClassName('modal-container')[0];
};
var modalHeaderHeight = function modalHeaderHeight() {
    return document.getElementsByClassName('modal-header')[0].getBoundingClientRect().height;
};
var modalBody = function modalBody() {
    return document.getElementsByClassName('modal-body')[0];
};
var pickHeightAreas = function pickHeightAreas(ev) {
    var wH = window.innerHeight;
    modalBox().style.height = modalContainer().style.height = wH - 20 + "px";
    modalBody().style.height = wH - modalHeaderHeight() - 40 + "px";
};
document.onloaded = pickHeightAreas();
window.onresize = function (ev) {
    var wH = window.innerHeight;
    modalBox().style.height = modalContainer().style.height = wH - 20 + "px";
    modalBody().style.height = wH - modalHeaderHeight() - 40 + "px";
};
//endregion