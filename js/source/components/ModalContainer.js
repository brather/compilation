/**
 * Created by AChechel on 31.05.2017.
 */
import React, {Component} from 'react';
import SourceArea from "./SourceArea";
import ContentTabs from "./ContentTabs";
import CompilationResource from "./CompilationResource";
import PreviewContentBlock from "./PerviewContentBlock";
import AppStorage from '../flux/AppStorage';

class ModalContainer extends Component {

    _renderSourceArea() {
        return (
            <SourceArea contentSource="Loading" />
        );
    }

    _renderTabs () {
        return (
            <ContentTabs />
        );
    }


    render () {
        _l('ModalContainer::render', this.state);

        return (<div>
            <div className="row">
                <div className="col-9">
                    {this._renderSourceArea()}
                </div>
                <div className="col-3 list-tabs-container">
                    {this._renderTabs()}
                </div>
            </div>
            <div className="row">
                <div className="col-12">
                    <h2>Новая статья</h2>
                </div>
            </div>
            <div className="row">
                <PreviewContentBlock />
            </div>
            <div className="row">
                <h3>Предпроспотр</h3>
                <div className="col-12">
                    <CompilationResource />
                </div>
            </div>
        </div>);
    }

    souldComponentUpdate () { return false; }
}

export default ModalContainer;