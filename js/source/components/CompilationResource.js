/**
 * Created by AChechel on 31.05.2017.
 */
import React, {Component} from 'react';
import TinyMCE from 'react-tinymce';
import AppStorage from "../flux/AppStorage";

class CompilationResource extends Component {
    constructor (props) {
        super(props);

        _l('CompilationResource::constructor');

        this.state = {
            resource: AppStorage.getRecord('compilationResource') || ""
        };

        AppStorage.addListener("compilationResource", () => {
            this.setState({
                resource: AppStorage.getRecord('compilationResource')
            })
        });
    }

    render () {
        _l('CompilationResource::render');

        return (
            <TinyMCE
                content={this.state.resource}
                config={{
                    height: 400,
                    menubar: false,
                    branding: false,
                    resize: false,
                }}
                onChange={this.handleChange.bind(this)}
                id={this.props.id}
                ref={this.props.refs}
            />
        );
    }
    handleChange(e) {
        let res = AppStorage.getRecord("compilationResource");
        let con = tinyMCE.EditorManager.get(this.props.id).getContent();

        if(res !== con)
            AppStorage.setData(con, "compilationResource", false);
    }

    shouldComponentUpdate(newProps, newState) {
        if (this.state.resource !== newState.resource) {
            tinyMCE.EditorManager.get(this.props.id).setContent(newState.resource);
            return true;
        }

        return false;
    }

    /*componentDidUpdate() {

        _l('CompilationResource::componentDidUpdate');

        if (this.props.resource){
            let tmpRes = tinyMCE.EditorManager.get(this.props.id).getContent() + this.props.resource;
            tinyMCE.EditorManager.get(this.props.id).setContent(tmpRes);
        }
    }*/
}

CompilationResource.defaultProps = {
    className: 'source-area',
    id: 'CompilationResource',
    refs: "compilationResource"
};

export default CompilationResource;