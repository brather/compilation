/**
 * Created by AChechel on 02.06.2017.
 */
import React, {Component} from 'react';
import AppStorage from "../flux/AppStorage";

class PreviewContentBlock extends Component {

    constructor(props) {
        super(props);

        this.state = {
            blocks: AppStorage.getRecord("previewBlock") || []
        };

        AppStorage.addListener("previewBlock", () => {
            this.setState({
                blocks: AppStorage.getRecord("previewBlock")
            });
        });
    }

    render () {
        _l("PreviewContentBlock::render");
        let previewBlocks = [];

        if (this.state.blocks && this.state.blocks.length) {

            for(let i = 0; i < this.state.blocks.length; i++) {

                previewBlocks.push(<div className="new-source-item" key={i}>
                    <div>{this.state.blocks[i]}</div>
                    <span className="new-source-item-number">{i + 1}</span>
                </div>);
            }
        } else {
            previewBlocks.push(<div className="empty-preview" key="0">Ничего не выбрано</div>)
        }

        return (<div className="col-12">{previewBlocks.map((v) => {return v;})}</div>);
    }
}

export default PreviewContentBlock;