/**
 * Created by AChechel on 29.05.2017.
 */
import React, {Component} from 'react';
import ContentUlLi from "./ContentUlLi";
import PropTypes from 'prop-types';
import AppStorage from "../flux/AppStorage";

class ContentTabs extends Component {
    constructor(props) {
        super(props);
        _l('ContentTabs::constructor');

        this.state = AppStorage.getRecord("contentTabs") || {};

        AppStorage.addListener("contentTabs", () => {
            this.setState(() => {
                return AppStorage.getRecord("contentTabs");
            });
        });
    }

    shouldComponentUpdate(newProp, newState) {
        _l('ContentTabs::shouldComponentUpdate', newProp, newState);
        return (this.state.count !== newState.count);
    }

    render() {
        _l('ContentTabs::render' , this.state);

        let list = [];

        for (let i = 0; i < this.state.count; i++) {
            let contentUlLi = <ContentUlLi
                article={this.state.articles[i]}
                key={i}
            />;
            /* сравнение строковых значений ID */
            if ( this.state.articles[i].id === AppStorage.param.SourceArea.originId ) {
                list.unshift(<ContentUlLi
                    article={this.state.articles[i]}
                    key={i}
                    className="active-li"
                />);
            } else {
                list.push(contentUlLi);
            }
        }

        return <ul className="content-tabs">{list.map((v) => v)}</ul>;
    }
}

ContentTabs.propsType ={
    articles: PropTypes.arrayOf({
        category_id: PropTypes.any,
        full_title: PropTypes.string,
        id: PropTypes.string,
        is_fuzzy: PropTypes.number,
        picture: PropTypes.any,
        preview: PropTypes.string,
        source_id: PropTypes.number,
        source_name: PropTypes.string,
        title: PropTypes.string
    }),
    count: PropTypes.number,
    total_count: PropTypes.number,
    originId: PropTypes.string
};

export default ContentTabs;