/**
 * Created by AChechel on 26.05.2017.
 */
import React, {Component} from 'react';
import TinyMCE from 'react-tinymce';
import AppStorage from '../flux/AppStorage';
import AppAction from "../flux/AppAction";

class SourceArea extends Component {

    constructor(props) {
        super(props);

        _l('SourceArea::constructor');

        this.state = AppStorage.getRecord("sourceArea") || {content: "Loading"};

        AppStorage.addListener("sourceArea", () => {
            this.setState(() => { return AppStorage.getRecord("sourceArea") || {content: "Что то пошло не так!" }});
        });
    }

    render () {
        _l('SourceArea::render');
        return (
            <TinyMCE
                content={this.state.content}
                config={{
                    height: 395,
                    menubar: false,
                    plugins: [],
                    toolbar: 'copy',
                    content_css: ['//fonts.googleapis.com/css?family=Lato:300,300i,400,400i','//www.tinymce.com/css/codepen.min.css'],
                    resize: false,
                    branding: false,
                    statusbar: false,
                }}
                onCopy={this.handleCopy.bind(this)}
                ref={this.props.refs}
                id={this.props.id}
            />
        );
    }

    handleCopy(e) {
        AppAction.addPreviewBlock(tinyMCE.EditorManager.get(this.props.id).selection.getContent());
        AppAction.appendContentCompilationResource('<p>' + tinyMCE.EditorManager.get(this.props.id).selection.getContent() + '</p>')
    }

    onSelection(e) {
        _l('SourceArea::onSelection target', e.target);
    }

    shouldComponentUpdate(newProps, newState) {
        _l('SourceArea::shouldComponentUpdate');
        if (newState.originId !== this.state.originId) {
            tinyMCE.EditorManager.get(this.props.id).setContent(newState.content);
            return true;
        }

        return false;
    }

    /*_handleUpdateState () {
        this.setState();
    }*/
}

SourceArea.defaultProps = {
    handleAppendResource: undefined,
    parse: {},
    refs: "sourceArea",
    className: "source-area",
    id: "source-area"
};

export default SourceArea;