/**
 * Created by AChechel on 18.05.2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import tinymce from 'tinymce';

class Text extends Component {
    constructor(props) {
        super(props);

        this.state = {
            porperty : {
                cols: this.props.cols,
                rows: this.props.rows,
                name: this.props.name,
                id: this.props.id,
            },
            value: this.props.defaultValue
        };
    }
    render() {

        //this._renderTinyMCE();
        const comm = this.state.property;

        return (<textarea {...comm} value={this.state.value} >{this.state.value}</textarea>);
    }

    _renderTinyMCE() {
        tinymce.init({
            selector: this.props.id,
            height: 500,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css'
        });
    }



}

Text.porpTypes = {
    name: PropTypes.string.isRequired,
    cols: PropTypes.string,
    rows: PropTypes.string,
    id: PropTypes.string,
    defaultValue: PropTypes.string
};

Text.defaultProps = {
    cols: "30",
    rows: "20"
};

export default Text