/**
 * Created by AChechel on 29.05.2017.
 */
import React, {Component} from "react";
import "whatwg-fetch";
import AppAction from "../flux/AppAction";

class ContentUlLi extends Component {

    render() {
        return (
            <li
                onClick={this._onClick.bind(this)}
                onTouchEnd={this._onClick.bind(this)}
                className={this.props.className || ""}
                data-id-source={this.props.article.id}>
                <h3>{this.props.article.title} {this.props.article.source_name}</h3>{this.props.article.preview + "..."}
            </li>);
    }

    _onClick (e) {
        /* Сохраняем \ Получаем значение в localStorage */
        if (e.currentTarget.className === "active-li") return false;

        AppAction.putContentOnSourceArea(e.currentTarget.dataset.idSource);
        let childred = e.currentTarget.parentElement.children;
        for( let i = 0 ; childred.length > i ; i++ ) {
            if (childred[i].className === "active-li")
                childred[i].className = "";
        }

        e.currentTarget.className = "active-li";
    }
}

export default ContentUlLi;