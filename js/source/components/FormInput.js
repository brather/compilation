/**
 * Created by AChechel on 15.05.2017.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Text from './Text';

class FormInput extends Component {
    getValue() {
        return 'value' in this.refs.input ? this.resf.input.value : this.refs.getValue();
    }
    render() {
        const common = {
            id: this.props.id,
            ref: 'input',
            defaultValue: this.props.defaultValue
        };

        switch (this.props.type) {
            case 'year':
                return (<input {...common} type="number" defaultValue={this.props.defaultValue || new Date().getFullYear()} />);
            case 'text':
                return <Text {...common} />;

            default:
                return <input {...common} type="text" />
        }
    }
}

FormInput.propTypes = {
    type: PropTypes.oneOf(['year','suggest','rating','text','input']),
    id: PropTypes.string,
    options: PropTypes.array,
    defaultValue: PropTypes.any,
    max: PropTypes.number
};


export default FormInput

