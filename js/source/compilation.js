'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import ModalContainer from "./components/ModalContainer";
import AppStorage from './flux/AppStorage';

//region/* Debug */
window.develop = false;
window._getLocal = () => JSON.parse(localStorage.getItem('data'));

window._l = (window.develop) ? console.log : () => void(0);
//endregion

AppStorage.init({
    SourceArea: {
        originId: "1354279"
    },
    ContentTabs: { /* Данные должны быть получены из контекста */
        title: "Гагарин Юрий Алексеевич",
        limit: 10,
        preview: 55
    }
});

ReactDOM.render(
    <div>
        <ModalContainer  />
    </div>,
    document.getElementById("compilation")
);
//region /* Module to resize modal container */
let modalBox            = () => document.getElementsByClassName('modal-box')[0];
let modalContainer      = () => document.getElementsByClassName('modal-container')[0];
let modalHeaderHeight   = () => document.getElementsByClassName('modal-header')[0].getBoundingClientRect().height;
let modalBody           = () => document.getElementsByClassName('modal-body')[0];
let pickHeightAreas     = (ev) => {
    let wH = window.innerHeight;
    modalBox().style.height = modalContainer().style.height = (wH - 20) + "px";
    modalBody().style.height = (wH - modalHeaderHeight() - 40) + "px";
};
document.onloaded = pickHeightAreas();
window.onresize = (ev) => {
    let wH = window.innerHeight;
    modalBox().style.height = modalContainer().style.height = (wH - 20) + "px";
    modalBody().style.height = (wH - modalHeaderHeight() - 40) + "px";
};
//endregion