/**
 * Created by AChechel on 06.06.2017.
 */
import AppStorage from "./AppStorage";
import "whatwg-fetch";

const AppAction = {
    addPreviewBlock(content) {
        let tmp = document.createElement('DIV');
        tmp.innerHTML = content;
        tmp = tmp.textContent.substr(0, 75) + "...";

        let data = AppStorage.getData();
        let arr = [];

        if ('previewBlock' in data) {
            arr = data.previewBlock;
        }

        if (arr.indexOf(tmp) < 0) {
            arr.push(tmp);
            AppStorage.setData(arr, 'previewBlock')
        }
    },

    /* localStorage Object contentSources */
    putContentOnSourceArea(sourceId) {
        let cont = AppStorage.getRecord('contentSources');
        if (!cont || !cont[sourceId]) {
            fetch("/api.php?action=parse&pageid=" + sourceId + "&format=json")
                .then( (response) => {          /* Check Status*/
                    if (response.status >= 200 && response.status < 300) {
                        return response
                    } else {
                        let error = new Error(response.statusText);
                        error.response = response;
                        throw error
                    }
                })
                .then( (parseJSON ) => {
                    return parseJSON.json();
                })
                .then( (data) => {
                    if (data.error) {
                        let error = new Error(data.error.info);
                        error.responce = data.error;
                        throw error;
                    } else {
                        let obj = {};
                        let sourceArea = AppStorage.getRecord("sourceArea");

                        if (cont) obj = cont;

                        obj[sourceId] = data.parse.text["*"];
                        if (sourceArea)
                            obj[sourceArea.originId] = sourceArea.content;

                        AppStorage.setData(obj, "contentSources", false);

                        AppStorage.setData({
                            content: obj[sourceId],
                            originId: sourceId
                        }, "sourceArea");
                    }
                });
        } else {
            let sourceArea = AppStorage.getRecord("sourceArea");
            cont[sourceArea.originId] = sourceArea.content;

            AppStorage.setData(cont, "contentSources", false);

            AppStorage.setData({
                content: cont[sourceId],
                originId: sourceId
            }, "sourceArea");
        }
    },

    appendContentCompilationResource(content) {
        let resource = AppStorage.getRecord('compilationResource');
        if (resource) {
            /* добавим контент если его еще нет в ресурсе */
            if (resource.indexOf(content) < 0) {
                content = resource + content;
            } else {
                return true;
            }
        }

        AppStorage.setData(content, 'compilationResource');

    }
};

export default AppAction;