/**
 * Created by AChechel on 06.06.2017.
 */
/** @flow */
import "whatwg-fetch";
import {EventEmitter} from 'fbemitter';

/** @type Array<Object> */
let data = {};

const emitter = new EventEmitter();

const AppStorage = {

    /**
     * @param eventType string
     * @param fn Function   */
    addListener(eventType, fn) {
        _l("AppStorage::addListener", eventType);
        emitter.addListener(eventType, fn);
    },

    /** @return Array<Object> */
    init(param) {
        this.param = param;

        const storage = 'localStorage' in window ? localStorage.getItem('data') : null;

        if (storage === null || storage === "null") {
            this._ajaxSource(param.SourceArea.originId);
            this._ajaxTabs(param.ContentTabs);
        } else {
            data = JSON.parse(storage);
            if (("sourceArea" in data && data.sourceArea.originId !== param.SourceArea.originId) || !("sourceArea" in data)){
                this._ajaxSource(param.SourceArea.originId);
            }

            if (!("contentTabs" in data)) {
                this._ajaxTabs(param.ContentTabs);
            }

            _l("AppStorage::init data, param", data, param);
        }
    },

    _ajaxTabs(param) {
        _l("start _ajaxTabs");
        let uri = "http://172.16.61.56/services/api/search/titles/search" +
                "?q=" + param.title +
                "&limit=" + param.limit +
                "&preview=" + param.preview ;
        fetch(uri)
            .then( (response) => {          /* Check Status*/
                if (response.status >= 200 && response.status < 300) {
                    return response
                } else {
                    let error = new Error(response.statusText);
                    error.response = response;
                    throw error
                }
            })
            .then( (parseJSON ) => {
                return parseJSON.json();

            })
            .then( (data) => {

                if (data.error) {
                    let error = new Error(data.error.info);
                    error.responce = data.error;
                    throw error;
                } else {
                    let obTabs = {
                        articles: data.articles,
                        count: data.count,
                        total_count: data.total_count
                    };
                    this.setData(obTabs, "contentTabs");
                    emitter.emit('contentTabs');
                }
            });

    },

    _ajaxSource(originID) {
        _l("start _ajaxSource");
        fetch("/api.php?action=parse&pageid=" + originID + "&format=json")
            .then( (response) => {          /* Check Status*/
                if (response.status >= 200 && response.status < 300) {
                    return response
                } else {
                    let error = new Error(response.statusText);
                    error.response = response;
                    throw error
                }
            })
            .then( (parseJSON ) => {
                return parseJSON.json();

            })
            .then( (data) => {

                if (data.error) {
                    let error = new Error(data.error.info);
                    error.responce = data.error;
                    throw error;
                } else {
                    let obText = {
                        content: data.parse.text["*"].replace(/<!--[\s\S]*?-->/gm, ''),
                        originId: originID
                    };
                    this.setData(obText, "sourceArea");
                    emitter.emit('sourceArea');
                }
            });
    },
    /** @return Array<Object> */
    getData() {
        return data;
    },

    /**
     *  @param newData Object
     *  @param key string
     *  @param commit boolean    */
    setData(newData, key, commit = true) {

        if ('localStorage' in window ) {
            data[key] = newData;
            localStorage.setItem('data', JSON.stringify(data));
            /* На каждый обект в localStorage data должен приходится свой Listener с именем key */
            if (commit) emitter.emit(key);
        }
    },

    /**
     * @param key number
     * @return Object|null */
    getRecord (key) {
        if (!data) return {};

        return (key in data) ? data[key] : null;
    },
};

export default AppStorage;